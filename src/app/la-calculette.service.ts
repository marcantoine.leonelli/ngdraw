import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LaCalculetteService {

  total: number = 0;

  constructor() { }

  plus(inc: number): void {
    this.total = this.total + inc ;
  }

}
