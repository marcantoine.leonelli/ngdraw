import { Component, OnInit } from '@angular/core';
import { FacadeDessinCommService } from '../facade-dessin-comm.service';
import { DessinInfo } from '../dessin-info';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-dessins',
  templateUrl: './liste-dessins.component.html',
  styleUrl: './liste-dessins.component.css'
})
export class ListeDessinsComponent implements OnInit{

  tableauDessin: DessinInfo[] = [];

constructor(private facadeDessin : FacadeDessinCommService, private route: Router) {}
  
  ngOnInit(): void {
    this.processRefreshArray();
  }

  processRefreshArray() : void {
    this.facadeDessin.getAllDessins().subscribe({
      next : (tab :DessinInfo[]) => {
        this.tableauDessin.splice(0, this.tableauDessin.length);
        this.tableauDessin.push(...tab);
      }
    });
  }

  doClickElement(id: string | undefined): void {
    console.log(id);
    if (id){
      this.route.navigate(["dessininfo", id]);
    }
  }


}
