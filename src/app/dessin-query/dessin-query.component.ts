import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DessinInfo } from '../dessin-info';
import { FacadeDessinCommService } from '../facade-dessin-comm.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dessin-query',
  templateUrl: './dessin-query.component.html',
  styleUrl: './dessin-query.component.css'
})
export class DessinQueryComponent {

  @Input("label") labelDuButton: string = "Load Dessin" ;

  @Output("dessinOK") tokenEvtGet : EventEmitter<DessinInfo> = new EventEmitter<DessinInfo>();

  lestyle = {border: '1px black solid'};

  lesclasses = {
    enerreur: false,
    commOK: false
  };

  idQuery: string = "0";

  constructor(private facadeDessin : FacadeDessinCommService
              , private route: Router
    ) {}

  doClick() : void{
    //this.lesclasses.avecunebordure = !this.lesclasses.avecunebordure;

    this.facadeDessin.getDessinById(this.idQuery).subscribe({
      next: (dessin: DessinInfo)=>{
        this.tokenEvtGet.emit(dessin);
        this.lesclasses.commOK = true;
        this.lesclasses.enerreur = false;
      },
      error : (err:any) => {
        this.lesclasses.commOK = false;
        this.lesclasses.enerreur = true;
      }
    });

    this.route.navigate(["dessininfo",this.idQuery]);


  }



}
