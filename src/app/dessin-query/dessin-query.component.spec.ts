import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DessinQueryComponent } from './dessin-query.component';

describe('DessinQueryComponent', () => {
  let component: DessinQueryComponent;
  let fixture: ComponentFixture<DessinQueryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DessinQueryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DessinQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
