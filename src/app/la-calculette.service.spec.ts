import { TestBed } from '@angular/core/testing';

import { LaCalculetteService } from './la-calculette.service';

describe('LaCalculetteService', () => {
  let service: LaCalculetteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LaCalculetteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
