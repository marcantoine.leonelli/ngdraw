import { TestBed } from '@angular/core/testing';

import { FacadeDessinCommService } from './facade-dessin-comm.service';

describe('FacadeDessinCommService', () => {
  let service: FacadeDessinCommService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FacadeDessinCommService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
