import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { AdministrationPageComponent } from './administration-page/administration-page.component';
import { DessinDisplayPageComponent } from './dessin-display-page/dessin-display-page.component';
import { ErrorPageComponent } from './error-page/error-page.component';

const routes: Routes = [
  { path : "" , redirectTo: "home" , pathMatch: 'full'},
  { path : "home" , component: HomePageComponent},
  { path : "admin" , component : AdministrationPageComponent},
  { path : "dessin" , component : DessinDisplayPageComponent},
  { path : "dessininfo/:id" , component : DessinDisplayPageComponent},
  { path : "**" , component : ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
