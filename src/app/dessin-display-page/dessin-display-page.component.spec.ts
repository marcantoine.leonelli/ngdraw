import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DessinDisplayPageComponent } from './dessin-display-page.component';

describe('DessinDisplayPageComponent', () => {
  let component: DessinDisplayPageComponent;
  let fixture: ComponentFixture<DessinDisplayPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DessinDisplayPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DessinDisplayPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
