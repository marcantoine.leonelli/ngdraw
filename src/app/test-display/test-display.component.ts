import { Component } from '@angular/core';
import { LaCalculetteService } from '../la-calculette.service';

@Component({
  selector: 'app-test-display',
  templateUrl: './test-display.component.html',
  styleUrl: './test-display.component.css'
})
export class TestDisplayComponent {

  uneData : string = "Valeur initiale";

  constructor(public laCalculette: LaCalculetteService) {}


  doPlus(): void {
    this.laCalculette.plus(1);
  }

}
