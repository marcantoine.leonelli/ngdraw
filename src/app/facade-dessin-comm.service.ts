import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DessinInfo } from './dessin-info';
import { Observable, delay, map, tap  } from 'rxjs';
import { environment } from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class FacadeDessinCommService {

  constructor(private http: HttpClient) { }

  creationDessin(newDessin: DessinInfo) : Observable<DessinInfo>{
    return this.http.post<DessinInfo>(`${environment.rootUrl}/dessins`, newDessin);
  }

  getAllDessins() : Observable<DessinInfo[]>{
    return this.http.get<DessinInfo[]>(`${environment.rootUrl}/dessins`);
  }

  getDessinById(id: string) : Observable<DessinInfo> {
    return this.http.get<DessinInfo>(`${environment.rootUrl}/dessins/${id}`).pipe(
      
      map((dessinTemp: DessinInfo) => {
        dessinTemp.daterecuperation = new Date().toISOString();
        return dessinTemp;
      })
      ,      
      tap((dessinTemp: DessinInfo) =>{
        console.log("recuperation du flux " , dessinTemp);
      })
      ,delay(2000)  
    );
  }

}
