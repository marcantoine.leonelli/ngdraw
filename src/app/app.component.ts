import { Component , OnInit } from '@angular/core';
import { FacadeDessinCommService } from './facade-dessin-comm.service';
import { DessinInfo } from './dessin-info';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit{


  title = 'SdvDraw';

  labelPourLeCmp : string ="label pour ID";

  constructor(private facadeDessin: FacadeDessinCommService) {}
  
  ngOnInit(): void {
    this.facadeDessin.getAllDessins().subscribe({
        next: (tab: DessinInfo[]) => {
          console.log(tab);
        },
        error(err: any) {
            console.error(err);
        },
    });

    this.facadeDessin.getDessinById("1").subscribe(
      {
        next: (dessin: DessinInfo) => {
          console.log("By ID" , dessin);
        },
        error : (err: any) =>{
          console.error(err);
        }
      }
    );

    this.facadeDessin.creationDessin({
      auteur: "toto",
      datecreation : new Date().toISOString(),
      content :  {}   
    }).subscribe({
      next : (dessin: DessinInfo)=> {
        console.log("creation " , dessin);
      },
      error : (err: any)=>{
        console.error(err);
      }
    });

  }

  doGetDessinOK(dessin: DessinInfo) : void {
    console.log("Depuis l'event " , dessin);
  }


}
