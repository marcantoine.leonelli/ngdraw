import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestDisplayComponent } from './test-display/test-display.component';
import { HttpClientModule } from '@angular/common/http';
import { DessinQueryComponent } from './dessin-query/dessin-query.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdministrationPageComponent } from './administration-page/administration-page.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { DessinDisplayPageComponent } from './dessin-display-page/dessin-display-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { MenuComponent } from './menu/menu.component';
import { ListeDessinsComponent } from './liste-dessins/liste-dessins.component';

@NgModule({
  declarations: [
    AppComponent,
    TestDisplayComponent,
    DessinQueryComponent,
    AdministrationPageComponent,
    ErrorPageComponent,
    DessinDisplayPageComponent,
    HomePageComponent,
    MenuComponent,
    ListeDessinsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
