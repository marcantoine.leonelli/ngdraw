export interface DessinInfo {
    id?: string;
    auteur: string;
    datecreation: string;
    content: any;
    daterecuperation?: string;
}
